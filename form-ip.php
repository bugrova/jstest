<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>AJAX-запросы</title>
</head>
<body>
<button type="submit" id="show">Показать айпи</button>
<div class="show-ip"></div>
<script src="js/showIp.js"></script>
</body>
</html>