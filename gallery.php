<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script:400,700&display=swap" rel="stylesheet">
    <title>JS</title>
</head>
<body>
    <div id="slider">
        <div class="photos">
            <img class="current" src="images/one.jpg" alt="">
            <img src="images/two.jpg" alt="">
            <img src="images/three.jpg" alt="">
            <img src="images/four.jpg" alt="">
            <img src="images/five.jpg" alt="">
            <img src="images/six.jpg" alt="">
            <img src="images/seven.jpg" alt="">
            <img src="images/eight.jpg" alt="">
        </div>
        <div class="buttons">
            <span class="prev">PREV</span>
            <span class="next">NEXT</span>
        </div>
    </div>
</body>
<script src="js/gallery.js"></script>
</html>
