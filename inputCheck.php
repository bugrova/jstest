<?php

$name = trim($_POST['name']);
$email = trim($_POST['email']);
$phone = trim($_POST['phone']);

if (($name == '') || ($email == '') || ($phone == '')) {
    echo 'Заполните все поля!';
} else if (! filter_var($email, FILTER_VALIDATE_EMAIL)) {
    echo 'Введите корректный e-mail адрес!';
} else {
    $data = 'Email: ' . $email . '; Name: ' . $name . '; Phone:' . $phone . '.\\n';
    file_put_contents('users.txt', $data);
    echo '1';
}
