window.onload =  function () {
    var name = document.querySelector('#name');
    var email = document.querySelector('#email');
    var phone = document.querySelector('#phone');

    document.querySelector('#send').onclick = function () {
        var params = 'name=' + name.value + '&' + 'phone=' + phone.value + '&' + 'email=' + email.value;

        ajaxPost(params);
    }
}

function ajaxPost(params) {
    var request = new XMLHttpRequest();

    request.onreadystatechange = function() {
        if (request.readyState === 4 && request.status === 200) {
            if (request.responseText === '1') {
                document.querySelector('#form').style.display = 'none';
                document.querySelector('#text-output').innerHTML = 'Все прошло успешно!';
            } else {
                document.querySelector('#text-output').innerHTML = request.responseText;
            }
        }
    }

    request.open('POST', 'inputCheck.php');
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    request.send(params);
}