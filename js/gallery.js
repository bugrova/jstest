var next = document.querySelector('#slider .buttons .next');
var prev = document.querySelector('#slider .buttons .prev');
var images = document.querySelectorAll('#slider .photos img');
var i = 0;

next.onclick = function () {
    images[i].className = '';
    i += 1;
    if (i >= images.length) {
        i = 0;
    }
    images[i].className = 'current';
}

prev.onclick = function () {
    images[i].className = '';
    i -= 1;
    if (i < 0) {
        i = images.length - 1;
    }
    images[i].className = 'current';
}