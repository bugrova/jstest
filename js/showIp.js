window.onload =  function () {
    document.querySelector('#show').onclick = function () {
        ajaxGet('ip.php', function(data) {
            document.querySelector('.show-ip').innerHTML = data;
        });
    }
}

function ajaxGet(url, callback) {
    var f = callback || function (data) {};

    var request = new XMLHttpRequest();

    request.onreadystatechange = function() {
        if (request.readyState === 4 && request.status === 200) {
            // document.querySelector('.show-ip').innerHTML = request.responseText;
            f(request.responseText);
        }
    }

    request.open('GET', url);
    request.send();
}